<?php

function username_to_personal_id_settings_form($form, $form_state) {
	$field = username_to_personal_id_adressfield_set();
	$countries = username_to_personal_id_country_options_list($field['field'], $field['instance']);
	$options = array();
	$defaults = array();
	$country = variable_get('site_default_country');
	$defaults['available_countries'][$country] = $country;
	foreach ($countries as $country_code => $country_name) {
		$options[$country_code] = array('country' => array('data' => array('#markup' => $country_name)));
		}
	$defaults = variable_get('username_to_personal_id_options', $defaults);
	$form['detail']['#markup'] = t('This settings will be altered with the user entity addressfield settings.');
	$form['default_country'] = array(
	  '#title' => t('Default Country'),
	  '#type' => 'select',
	  '#options' => $countries,
	  '#default_value' => array_key_exists('default_country', $defaults) ? $defaults['default_country'] : current($defaults['available_countries']),
	  );
	$form['available_countries'] = array(
	  '#type' => 'tableselect',
	  '#header' => array('country' => array('data' => t('Available Countries'))),
	  '#options' => $options,
	  '#default_value' => $defaults['available_countries'],
	  '#empty' => t('No content available.'),
	  );
	$form['actions'] = array(
		'#type' => 'actions', 
		'#weight' => 50, 
		);
	$form['actions']['submit'] = array(
		'#type' => 'submit', 
		'#value' => t('Save'),
		'#validate' => array('username_to_personal_id_settings_form_validate'),
		'#submit' => array('username_to_personal_id_settings_form_submit'), 
		'#weight' => 1, 
		);
	return $form;
	}
	
function username_to_personal_id_settings_form_validate($form, &$form_state) {
	if ($form_state['values']['available_countries'][$form_state['values']['default_country']] !== $form_state['values']['default_country']) {
		form_set_error('default_country', t('Default country needs to be part of the available countries'));
		}
	}
	
function username_to_personal_id_settings_form_submit($form, $form_state) {
	$countries = array();
	foreach ($form_state['values']['available_countries'] as $key => $value) {
		if ($key === $value) $countries[$key] = $value;
		}
	variable_set('username_to_personal_id_options', array(
		'default_country' => $form_state['values']['default_country'],
		'available_countries' => $countries,
		)
		);
	drupal_set_message(t('Defaults had been set.'));
	}
